# Oxygen Remaster icon set

An icon theme remaster of **Oxygen Icons**, originally designed by **The Oxygen Team** and used throughout the KDE 4+ era.
Folders have been replaced by their old version from the 4.6.2 version of the Oxygen Icons set.

The icon theme has been updated to support modern environments such as Gnome and Xfce.
Furthermore, some additional icons have been added.

## Credits

- Original Oxygen Icons source at [KDE's GitHub](https://github.com/KDE/oxygen-icons/)
- Oxygen Icons 4.6.2 folder icons taken from https://www.pling.com/p/1217074/#
- 16px Downloads folder variant added by me
- OpenOffice.org 3.2.1 icons retrieved from its [source package](https://archive.apache.org/dist/incubator/ooo/stable/3.2.1/OOo_3.2.1_src_core.tar.bz2)


# Installation

Copy the whole folder of the repository to either of the following locations:
- `/usr/share/icons` for usage of all users (requires root permissions)
- OR `~/.local/share/icons` for a single user on KDE systems
- OR `~/.icons` for single user on GTK systems like Mate, Gnome, Cinnamon, Xfce 


# Toolkit

This repository contains some tools to help expand and maintain the icon set.

## Dupe icons (symlink)

If you want to use an existing icon for a new application.

A) the icon is in the same category (e.g. 'apps'):
```
# ./dupe_icon.py <existing-icon-name> <new-icon-name>
./dupe_icon.py konqueror firefox
```
... will create an `apps/firefox.{png,svgz,svg}` symbolic link that points to `konqueror.{png,svgz,svg}` in each size directory

B) the existing icon is in a different category (e.g. 'devices') than the new one you want to create (e.g. 'apps'):
```
# ./dupe_icon.py <existing-icon-name> <new-icon-category> <new-icon-name>
./dupe_icon.py pda_blue apps handheld-manager
```
... will create an `apps/handeld-manager.{png,svgz,svg}` symbolic link that points to `../devices/pda_blue.{png,svgz,svg}` (relative path) within each size directory.

**NOTE:** always use `gtk-update-icon-cache <path-to-this-directory>` afterwards.

**NOTE:** use `copy_icon.py` instead of `dupe_icon.py` to create a genuine copy instead of a symlink.
