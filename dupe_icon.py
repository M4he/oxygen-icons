#!/usr/bin/env python3
import os
import sys

# args: <existing-icon-name> <new-icon-name>
#       OR:
#       <existing-icon-name> <new-category-name> <new-icon-name>

if len(sys.argv) < 3:
    print('wrong number of arguments')

if len(sys.argv) == 3:
    existing_ico = sys.argv[1]
    new_ico = sys.argv[2]
    new_category = None
else:
    existing_ico = sys.argv[1]
    new_category = sys.argv[2]
    new_ico = sys.argv[3]

root = '.'


def make_symlink(directory, target, linkname):
    previous_dir = os.getcwd()
    os.chdir(directory)
    try:
        os.symlink(target, linkname)
    except:
        print('linking failed')
    os.chdir(previous_dir)


# iterate over all size dirs
for size_dir_name in os.listdir(root):
    size_dir_path = os.path.join(root, size_dir_name)
    if os.path.isdir(size_dir_path) and not size_dir_name.startswith('.'):

        # iterate over all type dirs in the current size dir
        for type_dir_name in os.listdir(size_dir_path):
            type_dir_path = os.path.join(size_dir_path, type_dir_name)
            if os.path.isdir(type_dir_path):

                # iterate over all files within the current type dir
                for file_name in os.listdir(type_dir_path):
                    if file_name.endswith('.png') or \
                    file_name.endswith('.svg') or \
                    file_name.endswith('.svgz'):

                        pure_name, ext = os.path.splitext(file_name)
                        if pure_name == existing_ico:
                            source = os.path.join(type_dir_path, file_name)

                            # check if we need to link to a different type/category folder
                            if new_category and new_category != type_dir_name:
                                new_dir = os.path.join(size_dir_path, new_category)

                                # if target category doesn't exist for this size, skip
                                if not os.path.exists(new_dir) or not os.path.isdir(new_dir):
                                    print("%s not a directory, skipping..." % new_dir)
                                    continue

                                target = os.path.join(new_dir, new_ico + ext)
                                rel_src = os.path.join('..', type_dir_name, file_name)
                                print("%s -> %s (%s)" % (target, source, rel_src))
                                make_symlink(new_dir, rel_src, new_ico + ext)
                            else:
                                target = os.path.join(type_dir_path, new_ico + ext)
                                print("%s -> %s" % (target, source))
                                make_symlink(type_dir_path, file_name, new_ico + ext)







